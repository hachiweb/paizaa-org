<?php if(Auth::guard('company')->check()): ?>
<h3 class="seekertxt"><?php echo e(__('One million success stories')); ?>. <span><?php echo e(__('Search Jobseekers Today')); ?>.</span></h3>
<form action="<?php echo e(route('job.seeker.list')); ?>" method="get">
    <div class="searchbar">
		<div class="srchbox seekersrch">		
		<div class="input-group">
		  <input type="text"  name="search" id="empsearch" value="<?php echo e(Request::get('search', '')); ?>" class="form-control" placeholder="<?php echo e(__('Enter Skills or Job Seeker Details')); ?>" autocomplete="off" />
		  <span class="input-group-btn">
			<input type="submit" class="btn" value="<?php echo e(__('Search Job Seeker')); ?>">
		  </span>
		</div>
		</div>
		
       
        
    </div>
</form>
<?php else: ?>



	
		<div class="searchbar">
			<h3><?php echo e(__('One million success job stories')); ?>. <span><?php echo e(__('Start yours today')); ?>.</span></h3>
		<form action="<?php echo e(route('job.list')); ?>" method="get">		
		<div class="srchbox">
		
	
			<div class="srform">
				<label for=""> <?php echo e(__('Keywords / Job Title')); ?></label>
				<input type="text"  name="search" id="jbsearch" value="<?php echo e(Request::get('search', '')); ?>" class="form-control" placeholder="<?php echo e(__('Enter Skills or job title')); ?>" autocomplete="off" /></div>
			
		
				
		<div class="srcsubfld additional_fields">
		<div class="row">
			<div class="col-md-<?php echo e(((bool)$siteSetting->country_specific_site)? 12:6); ?>">
			
		
        <div class="srform">
			<label for=""><?php echo e(__('Select Functional Area')); ?></label>
            <?php echo Form::select('functional_area_id[]', ['' => __('Select Functional Area')]+$functionalAreas, Request::get('functional_area_id', null), array('class'=>'form-control', 'id'=>'functional_area_id')); ?>

        </div>
</div>	
        <?php if((bool)$siteSetting->country_specific_site): ?>
        <?php echo Form::hidden('country_id[]', Request::get('country_id[]', $siteSetting->default_country_id), array('id'=>'country_id')); ?>

        <?php else: ?>
		<div class="col-md-6">
        <div class="srform">
			<label for=""><?php echo e(__('Select Country')); ?></label>
            <?php echo Form::select('country_id[]', ['' => __('Select Country')]+$countries, Request::get('country_id', $siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')); ?>

        </div>
			</div>	
        <?php endif; ?>
		</div>
			
			
		<div class="row">
			<div class="col-md-6"><div class="srform">
			<label for=""><?php echo e(__('Select State')); ?></label>
            <span id="state_dd">
                <?php echo Form::select('state_id[]', ['' => __('Select State')], Request::get('state_id', null), array('class'=>'form-control', 'id'=>'state_id')); ?>

            </span>
        </div></div>	
			<div class="col-md-6"> <div class="srform">
			<label for=""><?php echo e(__('Select City')); ?></label>
            <span id="city_dd">
                <?php echo Form::select('city_id[]', ['' => __('Select City')], Request::get('city_id', null), array('class'=>'form-control', 'id'=>'city_id')); ?>

            </span>
        </div>	</div>	
		</div>
        
       
		
		</div>	
			
		<div class="srchbtn"><input type="submit" class="btn" value="<?php echo e(__('Search Job')); ?>"></div>
			
			
			
		</div>				
		</form>
		
    </div>
	
	
    

<?php endif; ?><?php /**PATH /var/www/html/job-portal/resources/views/includes/search_form.blade.php ENDPATH**/ ?>